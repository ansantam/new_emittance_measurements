# -------------------------------------------------------------------------
# Code to perform reference measurements in the PSB
# Andrea Santamaria Garcia
# -------------------------------------------------------------------------
import h5py
import os
import pyjapc
import sys
import time
import warnings

import numpy as np

warnings.filterwarnings("error")  # To handle warnings as exceptions

# Command line arguments ------------------------------------------------------
# Example of use:
# python launch_measurement.py PSB.USER.MD5 R2
user = str(sys.argv[1])  # PSB.USER.MD6 for example
ring = str(sys.argv[2])  # R1, R2, R3 or R4
plane = str(sys.argv[3])  # H or V
folder_name = str(sys.argv[4])  # beam type + measurement type

desired_shots = 15  # change this to the shots you want


# Time the measurement starts -------------------------------------------------
time_stamp = time.strftime("%Y_%m_%d_%H_%M")

print(" ")
print(">> Script launched at:", time_stamp)
print(" ")

# Create results folder -------------------------------------------------------
folder = os.path.join(os.getcwd(), folder_name + "_" + plane + "_" + ring + "_" + time_stamp)

if not os.path.exists(folder):
    os.makedirs(folder)

# Starting pyjapc -------------------------------------------------------------
japc = pyjapc.PyJapc(noSet=False)
japc.setSelector(user)


def record_bct(ring, shot):
    print("")
    print(">> Recording BCT")

    time_stamp_bct = time.strftime("%Y_%m_%d_%H_%M_%S")
    bct_file = h5py.File(os.path.join(folder, "bct_shot_" + str(shot) + "_" + ring + "_" + time_stamp_bct) + ".hdf5", "w")

    first_sample_time = japc.getParam("B" + ring + ".BCT-ST/Samples#firstSampleTime")
    intensity = japc.getParam("B" + ring + ".BCT-ST/Samples#samples")
    ctime = np.linspace(first_sample_time, first_sample_time + len(intensity) - 1, len(intensity))

    bct_file.create_dataset("ctime", data=ctime)
    bct_file.create_dataset("intensity", data=intensity)


def record_beta_gamma(ring, shot):
    print("")
    print(">> Recording gamma")

    time_stamp = time.strftime("%Y_%m_%d_%H_%M_%S")
    file = h5py.File(os.path.join(folder, "beta_gamma_" + str(shot) + "_" + ring + "_" + time_stamp) + ".hdf5", "w")

    beta = japc.getParam("BR.BETA-ST/Samples#samples")
    gamma = japc.getParam("BR.GAMMA-ST/Samples#samples")
    ctime = np.linspace(0, 1000, 10001)

    file.create_dataset("ctime", data=ctime)
    file.create_dataset("beta", data=beta)
    file.create_dataset("gamma", data=gamma)


def get_ws_profile(ring, plane, shot):
    print("")
    print(">> Recording wire scanner profile")
    time_stamp_ws = time.strftime("%Y_%m_%d_%H_%M_%S")

    try:
        data_x = japc.getParam("B" + ring + ".BWS.2L1." + plane + "_ROT" + "/Acquisition#projPositionSet1")
        data_y = japc.getParam("B" + ring + ".BWS.2L1." + plane + "_ROT" + "/Acquisition#projDataSet1")
        data_z = japc.getParam("B" + ring + ".BWS.2L1." + plane + "_ROT" + "/Acquisition#ctrTStampSet1")

        ws_file = h5py.File(os.path.join(folder, "wirescanner_shot_" + str(shot) + "_" + ring + "_" + time_stamp_ws) + ".hdf5", "w")

        ws_file.create_dataset("position", data=data_x)
        ws_file.create_dataset("time", data=data_z)
        ws_file.create_dataset("intensity", data=data_y)

    except:
        print(">> No wire scanner data. Waiting for next cycle.")


def get_semgrid(ring, shot):
    print("")
    print(">> Recording wire scanner profile")
    time_stamp_sem = time.strftime("%Y_%m_%d_%H_%M_%S")

    try:
        emittance = japc.getParam("BTM.BSFHV01/Emittance#emittance")

        sem_1_x = japc.getParam("BTM.BSFHV01/Acquisition#projPositionSet1")
        sem_1_y = japc.getParam("BTM.BSFHV01/Acquisition#projDataSet1")

        sem_2_x = japc.getParam("BTM.BSFHV02/Acquisition#projPositionSet1")
        sem_2_y = japc.getParam("BTM.BSFHV02/Acquisition#projDataSet1")

        sem_3_x = japc.getParam("BTM.BSFHV03/Acquisition#projPositionSet1")
        sem_3_y = japc.getParam("BTM.BSFHV03/Acquisition#projDataSet1")


        sem_file = h5py.File(os.path.join(folder, "semgrid_shot_" + str(shot) + "_" + ring + "_" + time_stamp_sem) + ".hdf5", "w")

        sem_file.create_dataset("emittance", data=emittance)

        sem_file.create_dataset("sem_1_x", data=sem_1_x)
        sem_file.create_dataset("sem_1_y", data=sem_1_y)

        sem_file.create_dataset("sem_2_x", data=sem_2_x)
        sem_file.create_dataset("sem_2_y", data=sem_2_y)

        sem_file.create_dataset("sem_3_x", data=sem_3_x)
        sem_file.create_dataset("sem_3_y", data=sem_3_y)

    except:
        print(">> Some problem with the SEMGRID data. Waiting for next cycle.")


def callback(param_name, new_value):
    # global tune_counter, bbq_flag, sampler_flag
    callback.counter += 1
    print("")
    print("-----------------------------------------------------------")
    print(">> Shot", callback.counter)
    # Put here the functions you want to use

    #record_beta_gamma(ring, callback.counter)

    if callback.counter % 2 == 1:
        #record_bct(ring, callback.counter)
        #get_semgrid(ring, callback.counter)
        japc.setParam("B" + ring + ".BWS.2L1." + plane + "_ROT" + "/Setting#mode", 1)

    if callback.counter % 2 == 0:
        record_bct(ring, callback.counter)
        get_semgrid(ring, callback.counter)
        time.sleep(0.5)
        get_ws_profile(ring, plane, callback.counter)
        

# Starting subscription to BCT, i.e. a measurement everytime the BCT value changes
japc.subscribeParam("B" + ring + ".BCT-ST/Samples", callback)
japc.startSubscriptions()

callback.counter = 0

while callback.counter < desired_shots - 1:
    time.sleep(20)
else:
    sys.exit(">> Measurements finished.")

japc.stopSubscriptions()
japc.clearSubscriptions()
