# -------------------------------------------------------------------------
# Code to perform reference measurements in the PSB
# Andrea Santamaria Garcia
# -------------------------------------------------------------------------
import collections
import datetime
import h5py
import os
import pyjapc
import re
import shutil
import sys
import time
import warnings

import numpy as np

warnings.filterwarnings("error")  # To handle warnings as exceptions

# Command line arguments ------------------------------------------------------
# Example of use:
# python launch_measurement.py PSB.USER.MD5 R2
user = str(sys.argv[1])  # PSB.USER.MD6 for example
ring = str(sys.argv[2])  # R1, R2, R3 or R4
folder_name = str(sys.argv[3])  # beam type + measurement type
tomoscope = sys.argv[4]  # T1 or T2

desired_shots = 3  # change this to the shots you want

# Time the measurement starts -------------------------------------------------
time_stamp = time.strftime("%Y_%m_%d_%H_%M")

print(" ")
print(">> Script launched at:", time_stamp)
print(" ")

# Create results folder -------------------------------------------------------
folder = os.path.join(os.getcwd(), folder_name + "_" + ring + "_" + time_stamp)

if not os.path.exists(folder):
    os.makedirs(folder)

# Starting pyjapc -------------------------------------------------------------
japc = pyjapc.PyJapc(noSet=False)
japc.setSelector(user)

def is_header(line):
    return re.search(r'#|@|%|\$|&', line) is not None


def get_line(infile):
    with open(infile, 'r') as data:
        for line in data:
            if is_header(line):
                continue
            else:
                yield line.strip('\n').split()


def get_column(infile):
    start_line = get_line(infile).__next__()
    data_dict = {key: [] for key, item in enumerate(start_line)}
    for line in get_line(infile):
        for count, item in enumerate(line):
            data_dict[count].append(float(item))
    return data_dict


def create_defaultdict(infile):
    data = get_column(infile)
    d = collections.defaultdict(dict)
    d['ctimes'] = data[0]
    d['delta_turns'] = data[1]
    d['n_traces'] = data[2]
    d['h_scale'] = data[3]
    d['n_samples'] = data[4]
    d['delay'] = data[5]
    d['v_scale'] = data[6]
    return d


def record_bct(ring, shot):
    print("")
    print(">> Recording BCT")

    time_stamp_bct = time.strftime("%Y_%m_%d_%H_%M_%S")
    bct_file = h5py.File(os.path.join(folder, "bct_shot_" + str(shot) + "_" + ring + "_" + time_stamp_bct) + ".hdf5", "w")

    first_sample_time = japc.getParam("B" + ring + ".BCT-ST/Samples#firstSampleTime")
    intensity = japc.getParam("B" + ring + ".BCT-ST/Samples#samples")
    ctime = np.linspace(first_sample_time, first_sample_time + len(intensity) - 1, len(intensity))

    bct_file.create_dataset("ctime", data=ctime)
    bct_file.create_dataset("intensity", data=intensity)


def get_device(tomoscope):
    device_number = [1, 2, 3, 4]
    device_type = ["A", "E", "T"]
    tomo_number = [1, 2]
    all_devices = []
    enabled_devices = []
    for dn in device_number:
        for dt in device_type:
            for tn in tomo_number:
                device_name = "BA" + str(dn) + "X." + dt + "TOMO-SU" + str(tn)
                #print(device_name, japc.getParam(device_name + "/OutEnable#outEnabled"))
                if japc.getParam(device_name + "/OutEnable#outEnabled") == 1:
                    all_devices.append(device_name)
    if len(all_devices) > 3:
        print(">> Two tomoscopes open.")
    for device in all_devices:
        if "SU" + tomoscope in device:
            enabled_devices.append(device)
    return enabled_devices


def set_tom_params(ring, time_stamp, enabled_devices, scope_channel, ctiming, delta_turns, n_traces, h_scale, n_samples, delay, v_scale):
    japc.setParam(enabled_devices[0] + "/Delay#delay", ctiming)
    japc.setParam(scope_channel.replace(".CH01", "").replace(".CH02", "") + "/TriggerCount#value", n_traces) # OasisScope class
    japc.setParam(enabled_devices[1] + "/Delay#delay", n_traces)
    japc.setParam(enabled_devices[2] + "/Delay#delay", delta_turns)
    hscale_range = japc.getParam(scope_channel.replace(".CH01", "").replace(".CH02", "") + "/TimeBase#range")
    japc.setParam(scope_channel.replace(".CH01", "").replace(".CH02", "") + "/TimeBase#value", n_samples*1e-9*h_scale) # OasisScope class
    japc.setParam(scope_channel.replace(".CH01", "").replace(".CH02", "") + "/Delay#value", delay*1e-9) # OasisScope class
    japc.setParam(scope_channel + "/Sensibility#value", v_scale*10) # OasisChannel class
    print(">> Ctime set to:", japc.getParam(enabled_devices[0] + "/Delay#delay"))
    print(">> Ctime in the tomoscope:", japc.getParam(scope_channel + "/Acquisition#CTime"))
    print(">> N traces set to:", japc.getParam(enabled_devices[1] + "/Delay#delay"))
    print(">> Delta turns set to:", japc.getParam(enabled_devices[2] + "/Delay#delay"))
    print(">> H. Scale set to:", japc.getParam(scope_channel.replace(".CH01", "").replace(".CH02", "") + "/TimeBase#value")*1e6)
    print(">> Delay set to:", int(japc.getParam(scope_channel.replace(".CH01", "").replace(".CH02", "") + "/Delay#value")*1e9))
    print(">> N Samples set to:", japc.getParam(scope_channel + "/NumberOfSamples#value"))
    print(">> V. Scale set to:", japc.getParam(scope_channel + "/Sensibility#value")*1e-1)


def get_bfield(ctime):
    folder_t = os.path.join(folder, "tomoscope")
    bfield = japc.getParam("BR.BFC-SC/OASISAcquisition#data")
    print(ctime)
    with open(os.path.join(folder_t, "bfield_" + ring + "_shot_"+ str(callback.counter) + "_ctime_" + str(int(d_tom['ctimes'][index]))+".txt"), 'w') as output:
        output.write(str(int(ctime)) + " " + "{:.12E}".format(bfield[int(ctime)*10]*1e-4) + '\n')


def copy_rename_autosave(old_file_name, new_file_name, ring, folder):
    if not os.path.exists(os.path.join(folder, "tomoscope")):
        os.makedirs(os.path.join(folder, "tomoscope"))
    folder_t = os.path.join(folder, "tomoscope")
    current_dir = os.getcwd()
    src_dir = "/tmp"
    dst_dir = folder_t
    src_file = os.path.join(src_dir, old_file_name)
    shutil.copy2(src_file, dst_dir)

    dst_file = os.path.join(dst_dir, old_file_name)
    new_dst_file_name = os.path.join(dst_dir, new_file_name)
    shutil.move(dst_file, new_dst_file_name)


def get_autosave_files():
    tmp_files = os.listdir("/tmp")
    recent_files = []
    for f in tmp_files:
        if f.startswith("autosave"):
            if datetime.datetime.fromtimestamp(os.path.getmtime(os.path.join("/tmp", f))).strftime("%Y_%m_%d_%H_%M") == datetime.datetime.fromtimestamp(time.time()).strftime("%Y_%m_%d_%H_%M"):
                recent_files.append(os.path.join("/tmp", f))
                # print(">> File belongs to current measurement", datetime.datetime.fromtimestamp(time.time()).strftime("%Y_%m_%d_%H_%M_%S"))
    return recent_files


# You need to know to which scope and channel the OASIS signal is going so that you can observe it.
# If the get_scope function fails, check the scope name by opening "Tomoscope: Control -> OASIS viewer -> Connections"
# To check the scopes associated to the tomoscope look into the BR1.BCW8L1-TOMO-AS device.
scopes_R1 = ["BR.SCOPE31.CH01", "BR.SCOPE33.CH01"]
scopes_R2 = ["BR.SCOPE31.CH02", "BR.SCOPE33.CH02"]
scopes_R3 = ["BR.SCOPE32.CH01", "BR.SCOPE34.CH01"]
scopes_R4 = ["BR.SCOPE32.CH02", "BR.SCOPE34.CH02"]

d_scopes = collections.defaultdict(dict)
d_scopes['R1']['T1'] = scopes_R1[0]
d_scopes['R1']['T2'] = scopes_R1[1]
d_scopes['R2']['T1'] = scopes_R2[0]
d_scopes['R2']['T2'] = scopes_R2[1]
d_scopes['R3']['T1'] = scopes_R3[0]
d_scopes['R3']['T2'] = scopes_R3[1]
d_scopes['R4']['T1'] = scopes_R4[0]
d_scopes['R4']['T2'] = scopes_R4[1]


d_alt_scopes = collections.defaultdict(dict)
d_alt_scopes['R1'] = "BR.SCOPE01.CH02"
d_alt_scopes['R2'] = "BR.SCOPE06.CH02"
d_alt_scopes['R3'] = "BR.SCOPE06.CH01"
d_alt_scopes['R4'] = "BR.SCOPE01.CH01"


# Print enabled devices
enabled_devices = get_device(tomoscope.strip('T'))
enabled_alt_devices = ["BA" + ring.strip("R") + "X.C-START-OAS", "BA" + ring.strip("R") + "X.INTERVAL-OAS", "BA" + ring.strip("R") + "X.NTRACES-OAS"]
print(" ")
print(">> Active devices in tomoscope", tomoscope, ":", enabled_devices)


scope_channel = d_scopes[ring][tomoscope]
print(" ")
print(">> Scope and channel currently used:", scope_channel)
print(" ")


if japc.getParam(d_scopes[ring]['T1'].replace(".CH01", "").replace(".CH02", "") + "/Standby#value") == 0 and japc.getParam(d_scopes[ring]['T2'].replace(".CH01", "").replace(".CH02", "") + "/Standby#value") == 0:
    sys.exit(">> Scope is OFF. Turn it on and try the script again. UNFREEZE.")

d_tom = create_defaultdict(os.path.join(os.getcwd(), 'settings', ring + '_TOM.txt'))


def callback(param_name, new_value):
    global index, profile_counter
    callback.counter += 1

    recent_files = get_autosave_files()

    print("")
    print("-----------------------------------------------------------")
    print(">> Shot", callback.counter)
    print(">> Profile counter", profile_counter)

    set_tom_params(ring, time_stamp, enabled_devices, scope_channel, d_tom['ctimes'][index], int(d_tom['delta_turns'][index]), int(d_tom['n_traces'][index]), d_tom['h_scale'][index], int(d_tom['n_samples'][index]), int(d_tom['delay'][index]), d_tom['v_scale'][index])

    if profile_counter < desired_shots:
        if japc.getParam(scope_channel + "/Acquisition#CTime") == japc.getParam(enabled_devices[0] + "/Delay#delay") or japc.getParam(scope_channel + "/Acquisition#CTime") == japc.getParam(enabled_devices[0] + "/Delay#delay") + 1 or japc.getParam(scope_channel + "/Acquisition#CTime") == japc.getParam(enabled_devices[0] + "/Delay#delay") - 1:

            if len(recent_files) == 0:
                print(">> No tomography files matching with the current time have been found. Wait for the next cycle.")

            elif len(recent_files) == 1:
                print(">> Copying", recent_files[0])
                copy_rename_autosave(max(recent_files, key=os.path.getctime), "tomogram_" + ring + "_shot_"+ str(callback.counter) + "_ctime_" + str(int(d_tom['ctimes'][index]))+".txt", ring, folder)
                get_bfield(d_tom['ctimes'][index])
                record_bct(ring, callback.counter)
                profile_counter += 1

            elif len(recent_files) > 1:
                print(">> Copying", max(recent_files, key=os.path.getctime))
                copy_rename_autosave(max(recent_files, key=os.path.getctime), "tomogram_" + ring + "_shot_"+ str(callback.counter) + "_ctime_" + str(int(d_tom['ctimes'][index]))+".txt", ring, folder)
                get_bfield(d_tom['ctimes'][index])
                record_bct(ring, callback.counter)
                profile_counter += 1

    else:
        index += 1
        profile_counter = 0

# Starting subscription to BCT, i.e. a measurement everytime the BCT value changes
japc.subscribeParam("B" + ring + ".BCT-ST/Samples", callback)
japc.startSubscriptions()

callback.counter = 0
index = 0
profile_counter = 0

while profile_counter < desired_shots * len(d_tom['ctimes']):
    time.sleep(20)
else:
    sys.exit(">> Measurements finished.")

japc.stopSubscriptions()
japc.clearSubscriptions()
