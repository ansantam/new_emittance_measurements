import collections
import glob
import h5py
import os
import re
import numpy as np
import matplotlib.gridspec as gridspec
import TwissTable as tt
from matplotlib import pyplot as plt
from scipy.optimize import curve_fit
from scipy import interpolate


def gauss(x, a, b, c, d, e):
    return a*np.exp(-(x - b) ** 2 / (2 * c ** 2)) + d * x + e


def get_beta(run, plane):
    data_path = os.path.join("madx", run)
    files = os.listdir(data_path)

    ctimes = []
    beta_x_h = []
    alpha_x_h = []
    beta_y_h = []
    alpha_y_h = []

    beta_x_v = []
    alpha_x_v = []
    beta_y_v = []
    alpha_y_v = []

    for f in files:
        if f.startswith("output"):
            ctime = re.search("R3_(.*)_0", f).group(1)
            file_path = os.path.join(data_path, f)
            tab = tt.TwissTable(file_path)
            tab.convertToNumpy()

            ws_h = tab.findDataIndex("NAME", "BR3.BWS.2L1.H_ROT")[0]

            ws_h_betx = tab.data["BETX"][ws_h]
            ws_h_bety = tab.data["BETY"][ws_h]
            ws_h_alphax = tab.data["ALFX"][ws_h]
            ws_h_alphay = tab.data["ALFY"][ws_h]
            ws_h_disp = tab.data["DISP"][ws_h]

            ctimes.append(ctime)
            beta_x_h.append(ws_h_betx)
            alpha_x_h.append(ws_h_alphax)
            beta_y_h.append(ws_h_bety)
            alpha_y_h.append(ws_h_alphay)


            ws_v = tab.findDataIndex("NAME", "BR3.BWS.2L1.V_ROT")[0]

            ws_v_betx = tab.data["BETX"][ws_v]
            ws_v_bety = tab.data["BETY"][ws_v]
            ws_v_alphax = tab.data["ALFX"][ws_v]
            ws_v_alphay = tab.data["ALFY"][ws_v]
            ws_v_disp = tab.data["DISP"][ws_v]

            beta_x_v.append(ws_v_betx)
            alpha_x_v.append(ws_v_alphax)
            beta_y_v.append(ws_v_bety)
            alpha_y_v.append(ws_v_alphay)

    if plane == "H":
        return ctimes, beta_x_h

    elif plane == "V":
        return ctimes, beta_y_v


def apply_fit(function, data_1, data_2, ind_var):

    mu_y = max(data_2)
    min_y = min(data_2)
    data_x = []
    data_y = []
    mu_x = []
    for x, y in zip(data_1, data_2):
        if y == mu_y:
            mu_x.append(x)
    for x, y in zip(data_1, data_2):
        if x - mu_x[0] > -40 and x - mu_x[0] < 40:
            data_x.append(x - mu_x[0])
            data_y.append((y - min_y) / mu_y)

    if ind_var == "space":
        try:
            popt, pcov = curve_fit(function, np.asarray(data_x), np.asarray(data_y))
            return np.asarray(data_x), np.asarray(data_y), popt, pcov
        except:
            print("")
            print("Fit of profile failed.")
            print("")
            return "NaN", "NaN"


def calculate_emittance(data_dict, ctimes_list, madx_folder, plane):

    emittance_dict = collections.defaultdict(list)

    for cctime in ctimes_list:
        sigma_wire = np.asarray(data_dict[(cctime, "sw")])
        sigma_grid_1 = np.asarray(data_dict[(cctime, "s1")])
        sigma_grid_2 = np.asarray(data_dict[(cctime, "s2")])
        sigma_grid_3 = np.asarray(data_dict[(cctime, "s3")])
        intensity = np.asarray(data_dict[(cctime, "i")])

        # To remove later shots with very different intensities
        int_new = [x if (x > np.mean(intensity) - np.std(intensity)) else 0 for x in intensity]
        int_new = [x if (x < np.mean(intensity) + np.std(intensity)) else 0 for x in int_new]
        print("")
        print(">> CYCLE TIME:", cctime)
        print("Recorded intensities:", len(intensity))
        counter = 0
        for i in int_new:
            if i != 0:
                counter += 1
        print("Intensities after outlier exclusion:", counter)


        # Extracting SEMGRID data for the shot where the wire was not flying
        sigma_grid_1_ref = np.asarray(data_dict[(cctime, "s1_ref")])
        sigma_grid_2_ref = np.asarray(data_dict[(cctime, "s2_ref")])
        sigma_grid_3_ref = np.asarray(data_dict[(cctime, "s3_ref")])
        intensity_ref = np.asarray(data_dict[(cctime, "i_ref")])

        int_new_ref = [x if (x > np.mean(intensity_ref) - 2*np.std(intensity_ref)) else 0 for x in intensity_ref]
        int_new_ref = [x if (x < np.mean(intensity_ref) + 2*np.std(intensity_ref)) else 0 for x in int_new_ref]

        # Extracting the beta function along the cycle
        ct_ws, beta_ws = get_beta(madx_folder, plane)
        fun_beta = interpolate.interp1d(np.asarray(ct_ws, dtype="float64"), beta_ws, kind='linear', fill_value='extrapolate')
        beta_wire = fun_beta(cctime)
        print("Beta wire:", beta_wire)

        # Extracting the relativistic parameters along the cycle
        data = h5py.File("beta_gamma.hdf5", 'r')
        ctime = np.asarray(list(data["ctime"]))
        beta = np.asarray(list(data["beta"]))
        gamma = np.asarray(list(data["gamma"]))

        beta_gamma = []

        for ct, b, g in zip(ctime, beta, gamma):
            if str(int(ct)) == cctime:
                beta_gamma.append(b)
                beta_gamma.append(g)

        beta_rel = beta_gamma[0]
        gamma_rel = beta_gamma[1]
        print("Beta rel:", beta_rel, "Gamma rel:", gamma_rel)

        # Calculating the emittance at the SEMGRIDS
        # The distance between the grids is 2.500 according to layout
        # They have been corrected according to the survey measurements. Minimal impact anyway

        beta_gamma_sem = []
        for ct, b, g in zip(ctime, beta, gamma):
            if str(int(ct)) == "796":
                beta_gamma_sem.append(b)
                beta_gamma_sem.append(g)

        beta_rel_sem = beta_gamma_sem[0]
        gamma_rel_sem = beta_gamma_sem[1]

        La = 0.000      # m
        Lb = La + 2.485 # m
        Lc = Lb + 2.487 # m

        Row_1 = [1, -2*La, La**2]
        Row_2 = [1, -2*Lb, Lb**2]
        Row_3 = [1, -2*Lc, Lc**2]

        # Create the matrix M
        M = np.matrix([Row_1, Row_2, Row_3])
        x = [sigma_grid_1**2, sigma_grid_2**2, sigma_grid_3**2]
        out = np.linalg.solve(M, x)

        # twiss parameters
        beta_x_emitRMS = out[0]
        alpha_x_emitRMS = out[1]
        gamma_x_emitRMS = out[2]

        # RMS Emittance
        try:
            emitRMS = np.sqrt(beta_x_emitRMS*gamma_x_emitRMS - alpha_x_emitRMS**2)

            # Normalized Emittance
            emittance_semgrid = emitRMS * beta_rel_sem * gamma_rel_sem
            #print(">>> SEMGRID emittance", emittance_semgrid)

            # twiss parameter
            betaTwiss  = beta_x_emitRMS / emitRMS
            alphaTwiss = alpha_x_emitRMS/ emitRMS
        except:
            print("Negative value enconuntered probably, WS.")


        # Calculating the emittance from the reference shot

        x_ref = [sigma_grid_1_ref**2, sigma_grid_2_ref**2, sigma_grid_3_ref**2]
        out_ref = np.linalg.solve(M, x_ref)

        # twiss parameters
        beta_x_emitRMS_ref = out_ref[0]
        alpha_x_emitRMS_ref = out_ref[1]
        gamma_x_emitRMS_ref = out_ref[2]

        # RMS Emittance
        try:
            emitRMS_ref = np.sqrt(beta_x_emitRMS_ref*gamma_x_emitRMS_ref - alpha_x_emitRMS_ref**2)

            # Normalized Emittance
            emittance_semgrid_ref = emitRMS_ref * beta_rel_sem * gamma_rel_sem
            #print(">>> SEMGRID emittance ref", emittance_semgrid_ref)

            # twiss parameter
            betaTwiss_ref  = beta_x_emitRMS_ref / emitRMS_ref
            alphaTwiss_ref = alpha_x_emitRMS_ref/ emitRMS_ref
        except:
            print("Negative value enconuntered probably.")

        # Appending values
        new_em = []
        new_sem_em = []
        new_int = []
        new_sig = []
        for i, s, e in zip(int_new, sigma_wire, emittance_semgrid):
            if i != 0:
                new_em.append((s**2/beta_wire) * beta_rel * gamma_rel)
                new_sem_em.append(e)
                new_int.append(i)
                new_sig.append(s)

        new_sem_em_ref = []
        new_int_ref = []
        for ir, er in zip(int_new_ref, emittance_semgrid_ref):
            if ir != 0:
                new_sem_em_ref.append(er)
                new_int_ref.append(ir)

        print(">> WS emittance:", np.asscalar(round(np.mean(new_em), 3)), "+/-", np.asscalar(round(np.std(new_em), 3)))
        print(">> SEM emittance wire:", np.asscalar(round(np.mean(new_sem_em), 3)), "+/-", np.asscalar(round(np.std(new_sem_em), 3)))
        print(">> SEM emittance no wire:", np.asscalar(round(np.mean(new_sem_em_ref), 3)), "+/-", np.asscalar(round(np.std(new_sem_em_ref), 3)))

        emittance_dict[(str(cctime), "beta_wire")].append(beta_wire.item())
        emittance_dict[(str(cctime), "beta_rel")].append(beta_rel)
        emittance_dict[(str(cctime), "gamma_rel")].append(gamma_rel)
        emittance_dict[(str(cctime), "emittance")].append(np.asscalar(np.mean(new_em)))
        emittance_dict[(str(cctime), "emittance_err")].append(np.asscalar(np.std(new_em, dtype=np.float64)))
        emittance_dict[(str(cctime), "intensity")].append(np.asscalar(np.mean(int_new)))
        emittance_dict[(str(cctime), "em_sem")].append(np.asscalar(np.mean(new_sem_em)))
        emittance_dict[(str(cctime), "em_sem_err")].append(np.asscalar(np.std(new_sem_em, dtype=np.float64)))
        emittance_dict[(str(cctime), "em_sem_ref")].append(np.asscalar(np.mean(new_sem_em_ref)))
        emittance_dict[(str(cctime), "em_sem_err_ref")].append(np.asscalar(np.std(new_sem_em_ref, dtype=np.float64)))
        emittance_dict[(str(cctime), "sigma")].append(np.asscalar(np.mean(new_sig)))
        emittance_dict[(str(cctime), "sigma_err")].append(np.asscalar(np.std(new_sig, dtype=np.float64)))

    return emittance_dict


def get_data(data_path):

    bct_files = glob.glob(os.path.join(data_path, "bct*"))
    ws_files = glob.glob(os.path.join(data_path, "wirescanner*"))
    sem_files = glob.glob(os.path.join(data_path, "semgrid*"))

    bct_dict = collections.defaultdict(list)
    ws_dict = collections.defaultdict(list)
    sem_dict = collections.defaultdict(list)
    data_dict = collections.defaultdict(list)

    shots_bct = []

    shot_ctimes_ws = []
    shot_ctimes_sem = []

    for fb in bct_files:
        data_bct = h5py.File(fb, 'r')
        x = np.asarray(list(data_bct["ctime"]))
        y = np.asarray(list(data_bct["intensity"]))
        shot_bct = re.search('shot_(.*)_R', fb).group(1)
        try:
            shots_bct.append(shot_bct)
            bct_dict[(str(shot_bct), "x")] = x
            bct_dict[(str(shot_bct), "y")] = y
        except:
            print(">> Problem with BCT file", shot_bct)

    for fw in ws_files:
        data_ws = h5py.File(fw, 'r')
        shot_ws = re.search('shot_(.*)_R', fw).group(1)
        ctime_ws = re.search('ctime_(.*)_shot', fw).group(1)
        try:
            shot_ctimes_ws.append((shot_ws, ctime_ws))
            ws_dict[(str(shot_ws), str(ctime_ws), "x")] = np.asarray(list(data_ws["position"]))
            ws_dict[(str(shot_ws), str(ctime_ws), "y")] = np.asarray(list(data_ws["intensity"]))
            ws_dict[(str(shot_ws), str(ctime_ws), "z")] = np.asarray(list(data_ws["time"]))
        except:
            print(">> Problem with WS file", shot_ctimes_ws)

    for fs in sem_files:
        data_sem = h5py.File(fs, 'r')
        x1 = np.asarray(list(data_sem["sem_1_x"]))
        y1 = np.asarray(list(data_sem["sem_1_y"]))
        x2 = np.asarray(list(data_sem["sem_2_x"]))
        y2 = np.asarray(list(data_sem["sem_2_y"]))
        x3 = np.asarray(list(data_sem["sem_3_x"]))
        y3 = np.asarray(list(data_sem["sem_3_y"]))
        shot_sem = re.search('shot_(.*)_R', fs).group(1)
        ctime_sem = re.search('ctime_(.*)_shot', fs).group(1)
        try:
            shot_ctimes_sem.append((shot_sem, ctime_sem))
            sem_dict[(str(shot_sem), str(ctime_sem), "x1")] = x1
            sem_dict[(str(shot_sem), str(ctime_sem), "y1")] = y1
            sem_dict[(str(shot_sem), str(ctime_sem), "x2")] = x2
            sem_dict[(str(shot_sem), str(ctime_sem), "y2")] = y2
            sem_dict[(str(shot_sem), str(ctime_sem), "x3")] = x3
            sem_dict[(str(shot_sem), str(ctime_sem), "y3")] = y3
        except:
            print(">> Problem with SEM file", shot_ctimes_sem)

    shot_list = list(set(shot_ctimes_ws) & set(shot_ctimes_sem))

    # Creating the list of shots where the semgrid recorded without wire scanner
    ref_shot_list = [x for x in list(set(shot_ctimes_sem)) if x not in shot_list]

    for s, ctt in ref_shot_list:
        intensity_ref = []
        for x_bct, y_bct in zip(bct_dict[(s, "x")],  bct_dict[(s, "y")]):
            if str(int(x_bct)) == ctt:
                intensity_ref.append(y_bct)

        try:

            x1, y1, popt1, pcov1 = apply_fit(gauss, sem_dict[(s, ctt, "x1")], sem_dict[(s, ctt, "y1")], "space")
            perr1 = np.sqrt(np.diag(pcov1))
#                 fit1 = r"$\sigma={:.3f}\pm{:.3f}$".format(abs(popt1[2]), perr1[2])

            x2, y2, popt2, pcov2 = apply_fit(gauss, sem_dict[(s, ctt, "x2")], sem_dict[(s, ctt, "y2")], "space")
            perr2 = np.sqrt(np.diag(pcov2))
#                 fit2 = r"$\sigma={:.3f}\pm{:.3f}$".format(abs(popt2[2]), perr2[2])

            x3, y3, popt3, pcov3 = apply_fit(gauss, sem_dict[(s, ctt, "x3")], sem_dict[(s, ctt, "y3")], "space")
            perr3 = np.sqrt(np.diag(pcov3))

            data_dict[(str(ctt), "s1_ref")].append(popt1[2])
            data_dict[(str(ctt), "s2_ref")].append(popt2[2])
            data_dict[(str(ctt), "s3_ref")].append(popt3[2])
            data_dict[(str(ctt), "i_ref")].append(intensity_ref[0])

        except:
            print(">> Problem with fit or intensity", s, ctt)

    for shot, ct in shot_list:
        intensity = []
        for x_bct, y_bct in zip(bct_dict[(shot, "x")],  bct_dict[(shot, "y")]):
            if str(int(x_bct)) == ct:
                intensity.append(y_bct)

#         fig = plt.figure()
#         fig, (ax1, ax2, ax3) = plt.subplots(1, 3)
#         plt.rcParams["figure.figsize"] = [10, 3]
#         ax1.scatter(bct_dict[(shot, "x")], bct_dict[(shot, "y")], s=0.3, color="black", label=str(intensity[0]))
#         ax1.set_xlabel('ctime [ms]')
#         ax1.set_ylabel(r'Intensity [$10^{10}$]')
#         ax1.legend(fontsize=10, loc="upper right")


        try:
            x, y, popt, pcov = apply_fit(gauss, ws_dict[(shot, ct, "x")]*1e-3, ws_dict[(shot, ct, "y")], "space")
            perr = np.sqrt(np.diag(pcov))
#             fit = r"$\sigma={:.3f}\pm{:.3f}$".format(popt[2], perr[2])

            x1, y1, popt1, pcov1 = apply_fit(gauss, sem_dict[(shot, ct, "x1")], sem_dict[(shot, ct, "y1")], "space")
            perr1 = np.sqrt(np.diag(pcov1))
#                 fit1 = r"$\sigma={:.3f}\pm{:.3f}$".format(abs(popt1[2]), perr1[2])

            x2, y2, popt2, pcov2 = apply_fit(gauss, sem_dict[(shot, ct, "x2")], sem_dict[(shot, ct, "y2")], "space")
            perr2 = np.sqrt(np.diag(pcov2))
#                 fit2 = r"$\sigma={:.3f}\pm{:.3f}$".format(abs(popt2[2]), perr2[2])

            x3, y3, popt3, pcov3 = apply_fit(gauss, sem_dict[(shot, ct, "x3")], sem_dict[(shot, ct, "y3")], "space")
            perr3 = np.sqrt(np.diag(pcov3))
#                 fit3 = r"$\sigma={:.3f}\pm{:.3f}$".format(abs(popt3[2]), perr3[2])

#             ax2.scatter(x, y, s=0.3, color="black")
#             ax2.plot(x, gauss(x, popt[0], popt[1], popt[2], popt[3], popt[4]), label=fit, lw=0.8, color='green')
#             ax2.set_xlabel('Position [mm]')
#             ax2.set_ylabel(r'Normalized amplitude')
#             ax2.set_ylim([-0.05, max(y) + 0.2*max(y)])
#             ax2.set_title(str(ct) + ", shot " + str(shot))
#             ax2.legend(fontsize=10, loc="upper left")

#             ax3.scatter(x1, y1, s=0.5, color='blue')
#             ax3.plot(x1, gauss(x1, popt1[0], popt1[1], popt1[2], popt1[3], popt1[4]), label="Grid 1: " + fit1, lw=0.8, color='blue')
#             ax3.scatter(x2, y2, s=0.5, color='green')
#             ax3.plot(x2, gauss(x2, popt2[0], popt2[1], popt2[2], popt2[3], popt2[4]), label="Grid 2: " + fit2, lw=0.8, color='green')
#             ax3.scatter(x3, y3, s=0.5, color='orange')
#             ax3.plot(x3, gauss(x3, popt3[0], popt3[1], popt3[2], popt3[3], popt3[4]), label="Grid 3: " + fit3, lw=0.8, color='orange')
#             ax3.legend(fontsize=10, loc="upper left")

#             ax3.set_ylim([-0.05, 2])

#             ax3.set_xlabel('Position [mm]')
#             ax3.set_ylabel(r'Normalized amplitude')

    #         ax3.plot(sem_dict[(shot, "x1")], sem_dict[(shot, "y1")])
    #         ax3.plot(sem_dict[(shot, "x2")], sem_dict[(shot, "y2")])
    #         ax3.plot(sem_dict[(shot, "x3")], sem_dict[(shot, "y3")])

#             plt.tight_layout()
#             plt.show()

            data_dict[(str(ct), "sw")].append(popt[2])
            data_dict[(str(ct), "s1")].append(popt1[2])
            data_dict[(str(ct), "s2")].append(popt2[2])
            data_dict[(str(ct), "s3")].append(popt3[2])
            data_dict[(str(ct), "i")].append(intensity[0])

        except:
            print(">> Problem with fit or intensity", shot, ct)
#             print(intensity[0], popt[2])
#             ax2.scatter(x, y, s=0.3, color="black")
#             ax2.plot(x, gauss(x, popt[0], popt[1], popt[2], popt[3], popt[4]), label=fit, lw=0.8, color='green')
#             ax2.set_xlabel('Position [mm]')
#             ax2.set_ylabel(r'Normalized amplitude')
#             ax2.set_ylim([-0.05, max(y) + 0.2*max(y)])
#             ax2.set_title(str(ct) + ", shot " + str(shot))
#             ax2.legend(fontsize=10, loc="upper left")

    return data_dict, list(set([a[1] for a in shot_list]))
