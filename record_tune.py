# -------------------------------------------------------------------------
# Sample code to perform reference measurements in the PSB
# Andrea Santamaria Garcia
# -------------------------------------------------------------------------
import h5py
import os
import pyjapc
import sys
import time
import warnings

import numpy as np

warnings.filterwarnings("error")  # To handle warnings as exceptions

# Command line arguments ------------------------------------------------------
# Example of use:
# python launch_measurement.py PSB.USER.MD5 R2
user = str(sys.argv[1])  # PSB.USER.MD6 for example
ring = str(sys.argv[2])  # R1, R2, R3 or R4
folder_name = str(sys.argv[3])

if len(sys.argv[3]) > 0:
    plane = str(sys.argv[3])  # H or V, optional argument

desired_shots = 5  # change this to the shots you want


# Time the measurement starts -------------------------------------------------
time_stamp = time.strftime("%Y_%m_%d_%H_%M")

print(" ")
print(">> Script launched at:", time_stamp)
print(" ")

# Create results folder -------------------------------------------------------
folder = os.path.join(os.getcwd(), folder_name + "_" + ring + "_" + time_stamp)

if not os.path.exists(folder):
    os.makedirs(folder)

# Starting pyjapc -------------------------------------------------------------
japc = pyjapc.PyJapc(noSet=False)
japc.setSelector(user)

# Function definitions --------------------------------------------------------


def record_bct(ring, shot):
    print("")
    print(">> Recording BCT")

    time_stamp_bct = time.strftime("%Y_%m_%d_%H_%M_%S")
    bct_file = h5py.File(os.path.join(folder, "bct_shot_" + str(shot) + "_" + ring + "_" + time_stamp_bct) + ".hdf5", "w")

    first_sample_time = japc.getParam("B" + ring + ".BCT-ST/Samples#firstSampleTime")
    intensity = japc.getParam("B" + ring + ".BCT-ST/Samples#samples")
    ctime = np.linspace(first_sample_time, first_sample_time + len(intensity) - 1, len(intensity))

    bct_file.create_dataset("ctime", data=ctime)
    bct_file.create_dataset("intensity", data=intensity)


def record_tune_bbq(ring, bbq_sampling, shot):  # bbq_sampling = time interval between measurements (5 ms)
    print("")
    print(">> Recording BBQ")

    time_stamp_bbq = time.strftime("%Y_%m_%d_%H_%M_%S")
    bbq_file = h5py.File(os.path.join(folder, "bbq_shot_" + str(shot) + "_" + ring + "_" + time_stamp_bbq) + ".hdf5", "w")

    try:
        bbqH = japc.getParam("B" + ring + ".BQ-H" + "-ST/Samples#samples")
        bbqV = japc.getParam("B" + ring + ".BQ-V" + "-ST/Samples#samples")

        first_sample_V = japc.getParam("B" + ring + ".BQ-V" + "-ST/Samples#firstSampleTime")
        time_stamps_V = np.linspace(first_sample_V, first_sample_V + bbq_sampling * (len(bbqV) - 1), len(bbqV))

        first_sample_H = japc.getParam("B" + ring + ".BQ-H" + "-ST/Samples#firstSampleTime")
        time_stamps_H = np.linspace(first_sample_H, first_sample_H + bbq_sampling * (len(bbqH) - 1), len(bbqH))

        bbq_file.create_dataset("bbqH", data=bbqH)
        bbq_file.create_dataset("bbqV", data=bbqV)
        bbq_file.create_dataset("ctimeH", data=time_stamps_H)
        bbq_file.create_dataset("ctimeV", data=time_stamps_V)

        raw_file = h5py.File(os.path.join(folder, "RawData_shot_" + str(shot) + "_" + ring + "_" + time_stamp_bbq) + ".hdf5", "w")

        rawH = japc.getParam("B" + ring + ".BQ/Acquisition#rawDataH")
        rawV = japc.getParam("B" + ring + ".BQ/Acquisition#rawDataV")
        raw_file.create_dataset("rawH", data=rawH)
        raw_file.create_dataset("rawV", data=rawV)

    except:
        print("Empty BBQ values.")


def record_tune_sampler(ring, shot):  # data aligned with bbq one
    print("")
    print(">> Recording sampler")
    time_stamp_sampler = time.strftime("%Y_%m_%d_%H_%M_%S")

    first_sample_V = japc.getParam("B" + ring + ".QV" + ring.strip("R") + "-ST/Samples#firstSampleTime")
    tune_V = japc.getParam("B" + ring + ".QV" + ring.strip("R") + "-ST/Samples")['samples']
    time_stamps_V = np.linspace(first_sample_V - 1, first_sample_V + (len(tune_V)/10), len(tune_V))

    first_sample_H = japc.getParam("B" + ring + ".QH" + ring.strip("R") + "-ST/Samples#firstSampleTime")
    tune_H = japc.getParam("B" + ring + ".QH" + ring.strip("R") + "-ST/Samples")['samples']
    time_stamps_H = np.linspace(first_sample_H - 1, first_sample_H + (len(tune_H)/10), len(tune_H))

    sampler_file = h5py.File(os.path.join(folder, "sampler_shot_" + str(shot) + "_" + ring + "_" + time_stamp_sampler) + ".hdf5", "w")

    sampler_file.create_dataset("samplerH", data=tune_H)
    sampler_file.create_dataset("samplerV", data=tune_V)
    sampler_file.create_dataset("ctimeH", data=time_stamps_H)
    sampler_file.create_dataset("ctimeV", data=time_stamps_V)


def record_currents(ring, tune_counter):
    print(">> Recording currents")
    time_stamp_currents = time.strftime("%Y_%m_%d_%H_%M_%S")

    qfo = japc.getParam("BR.QFO-SA/SamplesFromMarker", dataFilterOverride={'marker':275.0, 'step':1.0, 'nSamples':530})['samples']
    qde = japc.getParam("BR.QDE-SA/SamplesFromMarker", dataFilterOverride={'marker':275.0, 'step':1.0, 'nSamples':530})['samples']
    mps = japc.getParam("BR.MPS-SA/SamplesFromMarker", dataFilterOverride={'marker':275.0, 'step':1.0, 'nSamples':530})['samples']
    bdl = japc.getParam("B" + ring +".BDL-SA/SamplesFromMarker", dataFilterOverride={'marker':275.0, 'step':1.0, 'nSamples':530})['samples']
    if ring == "R1":
        qcf = japc.getParam("B" + ring +".GSQCF.IMEAS-SD/SamplesFromMarker", dataFilterOverride={'marker':275.0, 'step':1.0, 'nSamples':530})['samples']
    else:
        qcf = japc.getParam("B" + ring +".QCF-SA/SamplesFromMarker", dataFilterOverride={'marker':275.0, 'step':1.0, 'nSamples':530})['samples']
    qcd = japc.getParam("B" + ring +".QCD-SA/SamplesFromMarker", dataFilterOverride={'marker':275.0, 'step':1.0, 'nSamples':530})['samples']

    current_file = h5py.File(os.path.join(folder, "currents_shot_" + str(tune_counter) + "_" + ring + "_" + time_stamp_currents) + ".hdf5", "w")

    current_file.create_dataset("qfo", data=qfo)
    current_file.create_dataset("qde", data=qde)
    current_file.create_dataset("mps", data=mps)
    current_file.create_dataset("bdl", data=bdl)
    current_file.create_dataset("qcf", data=qcf)
    current_file.create_dataset("qcd", data=qcd)
    current_file.create_dataset("ctime", data=np.arange(275, 805))

def callback(param_name, new_value):
    # global tune_counter, bbq_flag, sampler_flag
    callback.counter += 1
    print("")
    print("-----------------------------------------------------------")
    print(">> Shot", callback.counter)
    # Put here the functions you want to use
    record_bct(ring, callback.counter)
    record_tune_bbq(ring, japc.getParam("B" + ring + ".BQ/Setting#acqPeriod"), callback.counter)
    record_tune_sampler(ring, callback.counter)
    record_currents(ring, callback.counter)


# Starting subscription to BCT, i.e. a measurement everytime the BCT value changes
japc.subscribeParam("B" + ring + ".BCT-ST/Samples", callback)
japc.startSubscriptions()

callback.counter = 0


while callback.counter < desired_shots - 1:
    time.sleep(20)
else:
    sys.exit(">> Measurements finished.")

japc.stopSubscriptions()
japc.clearSubscriptions()
