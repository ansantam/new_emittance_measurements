# -------------------------------------------------------------------------
# Code to perform static tune scans in the PSB
# Developed by Andrea Santamaria Garcia
# -------------------------------------------------------------------------
import h5py
import os
import pyjapc
import random
import sys
import time
import warnings

import numpy as np

warnings.filterwarnings("error")  # To handle warnings as exceptions

# Command line arguments
# Example of use: python get_tune_scan.py PSB.USER.MD5 R2 H minmax
user = str(sys.argv[1])  # PSB.USER.MD6 for example
ring = str(sys.argv[2])  # R1, R2, R3 or R4

desired_shots = 2

# Measuring cycle period in miliseconds
start_time = 450
end_time = 600
bbq_sampling = 10

# Time of the start of the scan
time_stamp = time.strftime("%Y_%m_%d_%H_%M")

print(" ")
print(">> Script launched at:", time_stamp)
print(" ")



# Starting pyjapc
japc = pyjapc.PyJapc(noSet=False)
japc.setSelector(user)



# Function definitions

def change_main_tune():
    
    blowup_resonance = 4.19
    tune_h = 4.3
    tune_v = 4.46
    start_blowup = 280
    step_in_blowup = 5
    blow_up_duration = 1
    step_out_blowup = 5
    before_measurement = 20
   

    japc.setParam("rmi://inca_psb/PSBBEAM/QH#value", [[[275, start_blowup, start_blowup + step_in_blowup, start_blowup + step_in_blowup +  blow_up_duration, start_blowup + step_in_blowup +  blow_up_duration + step_out_blowup, start_time - before_measurement, start_time, end_time, 805],
                                                       [tune_h, tune_h,  blowup_resonance,  blowup_resonance, 4.25, 4.25, 4.25, 4.25, 4.25]]])

    japc.setParam("rmi://inca_psb/PSBBEAM/QV#value", [[[275, start_blowup, start_blowup + step_in_blowup, start_blowup + step_in_blowup +  blow_up_duration, start_blowup + step_in_blowup +  blow_up_duration + step_out_blowup, start_time - before_measurement, start_time, end_time, 805],
                                                       [tune_v, tune_v,  blowup_resonance,  blowup_resonance, 4.45, 4.45, 4.45, 4.45, 4.45]]])



def change_main_tune_simple():
    
    #tune_h = 4.25
    #tune_v = 4.45

    tune_h = 4.3
    tune_v = 4.46

    japc.setParam("rmi://inca_psb/PSBBEAM/QH#value", [[[275, 800],
                                                       [tune_h, tune_h]]])


    japc.setParam("rmi://inca_psb/PSBBEAM/QV#value", [[[275, 800],
                                                       [tune_v, tune_v]]])
    

def callback(param_name, new_value):
    callback.counter += 1
    print("")
    print("-----------------------------------------------------------")
    print(">> Shot", callback.counter)
    #try:
    change_main_tune_simple()
    #except:
       # print(">> Trim exception, going to next working point and ignoring this one.")
        #print("")
       

# Starting subscription
japc.subscribeParam("B" + ring + ".BCT-ST/Samples", callback)
japc.startSubscriptions()

callback.counter = 0

while callback.counter < desired_shots - 1:
    time.sleep(20)
else:
    sys.exit(">> Tune changed.")

japc.stopSubscriptions()
japc.clearSubscriptions()
