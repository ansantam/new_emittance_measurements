/******************************************************************************************
*
* MAD-X File for PS Booster optics calculations
*
* Execute with:  >madx < psb_orbit.madx
*
******************************************************************************************/

 title, 'BOOSTER lattice';

 option, echo;
 option, RBARC=FALSE;

/******************************************************************************************
 * BOOSTER
 ******************************************************************************************/
 call, file = 'psb.ele';
 call, file = 'psb.seq';
 call, file = 'psb.dbx';
 call, file = 'psb_orbit.str';

/******************************************************************************************
 * beam, use
 ******************************************************************************************/
call, file = 'psb_orbit_755_0.beamx';
use, sequence=psb3;

set,  format="20.10f";

/******************************************************************************************
 * Match for new working point
 ******************************************************************************************/

MATCH,sequence=psb3;
 vary, NAME=kKF, step = 0.0001;
 vary, NAME=kKD, step = 0.0001;
 constraint, range=#E, MUX=4.194894, MUY=4.1561;
 lmdif, calls = 10000, tolerance = 1.0E-21;
ENDMATCH;


! Print results on file: match_orbit.prt
assign, echo="match_orbit.prt";
print, text="match_orbit";
value,  kKF;
value,  kKD;
assign, echo=terminal;

/******************************************************************************************
 * TWISS
 ******************************************************************************************/

PSHIFT=0;

/************************************************************
 * MAD uses pt as the 5th variable NOT delta-p.
 * The result is that all derivatives are with repect to pt.
 * This is the reason for the non-standard dispersion values
 * for non-relativistic machines, like the PSB
 ************************************************************/
 beta=sqrt(1-1/beam->gamma^2);
 disp:=beta*table(twiss,dx); ! Delta_x=Disp*Delta_p/p;

 select, flag=twiss, clear;
 select, flag=twiss, column=name, s,x,alfx,alfy,betx,bety,disp,beta11,beta22,disp1;

 twiss ,centre
       , DELTAP = PSHIFT
       , table=TWISS
       , file='output_R3_755_0.tfs';

STOP;
