import collections
import glob
import h5py
import os
import re
import subprocess
import sys

import numpy as np


# -----------------------------------------------------------------------------

# Function definitions ---------------------------------------------------------


def is_header(line):
    return re.search(r'#|@|%|\$|&', line) is not None


def get_line(infile):
    with open(infile, 'r') as data:
        for line in data:
            if is_header(line):
                continue
            else:
                yield line.strip('\n').split()


def get_column(infile):
    start_line = get_line(infile).__next__()
    data_dict = {key: [] for key, item in enumerate(start_line)}
    for line in get_line(infile):
        for count, item in enumerate(line):
            data_dict[count].append(float(item))
    return data_dict


def get_csv_row(infile):
    with open(infile, 'r') as data:
        for line in data:
            if is_header(line):
                continue
            else:
                line_list = line.strip(',').split()
                if int(len(line_list)) == 0:
                    continue
                elif any(item.strip('.').isalpha() for item in line_list) is False:
                    yield line_list


def get_csv_column(infile):
    generator = get_csv_row(infile)
    data_dict = {key: [] for key, item in enumerate(generator.__next__())}
    for line in generator:
        for count, item in enumerate(line):
            data_dict[count].append(float(item))
    return data_dict


def get_tunes(infile):
    bbq_files = glob.glob(os.path.join("tunes", infile))
    bbq_dict = collections.defaultdict(list)

    for fw in bbq_files:
        data_bbq = h5py.File(fw, 'r')
        bbq_dict["xh"] = np.asarray(list(data_bbq["ctimeH"]))
        bbq_dict["yh"] = np.asarray(list(data_bbq["bbqH"])) + 4
        bbq_dict["xv"] = np.asarray(list(data_bbq["ctimeV"]))
        bbq_dict["yv"] = np.asarray(list(data_bbq["bbqV"])) + 4
    return bbq_dict


def get_momentum():
    data_dict = get_csv_column("../cycle_energy_rel.csv")
    return np.asarray(data_dict[0]), np.asarray(data_dict[4])*1e-3


def copy_files(name):
    subprocess.call(["mkdir", name])
    subprocess.call(["cp", "psb_new.seq", name])
    subprocess.call(["cp", "psb_orbit.beamx", name])
    subprocess.call(["cp", "psb_orbit.madx", name])
    subprocess.call(["cp", "psb_orbit.str", name])
    subprocess.call(["cp", "psb.dbx", name])
    subprocess.call(["cp", "psb.ele", name])
    subprocess.call(["cp", "psb.seq", name])
    subprocess.call(["cp", "madx", name])
    subprocess.call(["chmod", "+x", "madx"])


def modify_madx_file(name, ring, ctimes, tune_x, tune_y, momentum):

    for t, qx, qy, p in zip(ctimes, tune_x, tune_y, momentum):
        with open(os.path.join(os.getcwd(), name, "psb_orbit.madx"), "rt") as fin:
            with open(os.path.join(os.getcwd(), name, "psb_orbit_" + ring + "_" + str(t).replace(".", "_") + ".madx"), "wt") as fout:
                for line in fin:
                    fout.write(line.replace("TUNE_X", str(qx)).replace("TUNE_Y", str(qy)).replace("RING", "psb" + str(ring)).replace("output.tfs", "output_R" + str(ring) + "_" + str(t).replace(".", "_") + ".tfs").replace('psb_orbit.beamx', 'psb_orbit_' + str(t).replace(".", "_") + '.beamx'))

        with open(os.path.join(os.getcwd(), name, "psb_orbit.beamx"), "rt") as fin2:
            with open(os.path.join(os.getcwd(), name, "psb_orbit_" + str(t).replace(".", "_") + '.beamx'), "wt") as fout2:
                for line2 in fin2:
                    fout2.write(line2.replace("MOMENTUM", str(p)))

        os.system("cd " + name + "/" + "\n./madx " + "psb_orbit_" + ring + "_" + str(t).replace(".", "_") + ".madx")

# ------------------------------------------------------------------------------


aligned_momentum = []
bbq_dict = get_tunes("bbq_run_VII.hdf5")
momentum_ct, momentum = get_momentum()

for xh, yh, xv, yv in zip(bbq_dict["xh"], bbq_dict["yh"], bbq_dict["xv"], bbq_dict["yv"]):
    for p_ct, p in zip(momentum_ct, momentum):
        if xh == p_ct:
            aligned_momentum.append(p)

copy_files("madx_run_VII")
modify_madx_file("madx_run_VII", "3", bbq_dict["xh"], bbq_dict["yh"], bbq_dict["yv"], aligned_momentum)
